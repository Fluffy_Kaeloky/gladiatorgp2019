#pragma once

#include "Components/ActorComponent.h"
#include "DamageFeedbackComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GLADIATORGP2019_API UDamageFeedbackComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
	UMaterialInterface* materialTemplate = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* meshInstance = nullptr;

	UPROPERTY(EditAnywhere)
	int materialIndex = 0;

public:	
	UDamageFeedbackComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	void PlayFeedback();

private:
	UMaterialInstanceDynamic* dynamicMaterialInstance = nullptr;
	float redStatus = 0.0f;
};
