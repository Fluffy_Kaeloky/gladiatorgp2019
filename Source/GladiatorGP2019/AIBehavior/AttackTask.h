#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "AttackTask.generated.h"


UCLASS()
class GLADIATORGP2019_API UAttackTask : public UBTTaskNode
{
	GENERATED_BODY()
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:

	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector AIKey;
	
	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector PlayerKey;
	
	float Distance = 400.f;
	
};
