#pragma once

#include "BehaviorTree/BTDecorator.h"
#include "IsAtTheGoodDistance.generated.h"

UCLASS()
class GLADIATORGP2019_API UIsAtTheGoodDistance : public UBTDecorator
{
	GENERATED_BODY()
	
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	
public:

	UPROPERTY(EditAnywhere)
		float Distance = 400.0f;
	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector PlayerKey;

};
