#include "GladiatorGP2019.h"
#include "StayAwayTask.h"
#include "PlayerMovementController.h"
#include "BehaviorTree/BlackboardComponent.h"


EBTNodeResult::Type UStayAwayTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	
	UStayAwayTask::bNotifyTick = true;

	return EBTNodeResult::InProgress;
}

void UStayAwayTask::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	UBlackboardComponent* BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard == nullptr)
	{
		return;
	}

	ACharacter* AI = Cast<ACharacter>(BlackBoard->GetValueAsObject(AIKey.SelectedKeyName));
	if (AI == nullptr)
	{
		return;
	}

	ACharacter* Player = Cast<ACharacter>(BlackBoard->GetValueAsObject(PlayerKey.SelectedKeyName));
	if (Player == nullptr)
	{
		return;
	}

	UPlayerMovementController * AIController = AI->FindComponentByClass<UPlayerMovementController>();

	FVector TargetPosition = Player->GetActorLocation();
	FVector Position = AI->GetActorLocation();
	FVector Direction = TargetPosition - Position;
	Direction.Normalize();
	AI->SetActorRotation(FMath::Lerp(AI->GetActorRotation(), Direction.Rotation(), 1.f));

	float Forward, Right;
	FVector ActorForward = AI->GetActorForwardVector();
	FVector ActorRight = AI->GetActorRightVector();

	Forward = FVector::DotProduct(-Direction, ActorForward);
	Right = FVector::DotProduct(-Direction, ActorRight);
	
	if (FVector::Dist(TargetPosition, Position) < Distance)
	{
		AIController->MoveForward(Forward * MoveSpeed);
		AIController->MoveRight(Right * MoveSpeed);
	}
	else if (FVector::Dist(TargetPosition, Position) >= Distance)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}

}

