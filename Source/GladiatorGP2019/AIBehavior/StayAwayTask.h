#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "StayAwayTask.generated.h"

UCLASS()
class GLADIATORGP2019_API UStayAwayTask : public UBTTaskNode
{
	GENERATED_BODY()
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

public:
	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector PlayerKey;

	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector AIKey;

private:
	UPROPERTY(EditAnywhere, Category = Movement)
		float Distance = 400.0f;

	UPROPERTY(EditAnywhere, Category = Movement)
		float MoveSpeed = 5.0f;
};
