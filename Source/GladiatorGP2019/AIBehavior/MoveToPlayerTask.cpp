#include "GladiatorGP2019.h"
#include "MoveToPlayerTask.h"
#include "BehaviorTree/BlackboardComponent.h"


EBTNodeResult::Type UMoveToPlayerTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	UBlackboardComponent* BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	AI = Cast<ACharacter>(BlackBoard->GetValueAsObject(AIKey.SelectedKeyName));
	if (AI == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ACharacter * Player = Cast<ACharacter>(BlackBoard->GetValueAsObject(PlayerKey.SelectedKeyName));
	if (Player == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	FVector TargetPosition = Player->GetActorLocation();
	FVector Position = AI->GetActorLocation();
	FVector Direction = TargetPosition - Position;
	Direction.Normalize();
	AI->SetActorRotation(FMath::Lerp(AI->GetActorRotation(), Direction.Rotation(), 1.f));
	
	bNotifyTick = true;


	if (FVector::Dist(TargetPosition, Position) > Distance)
	{
		return EBTNodeResult::InProgress;
	}
	

	return EBTNodeResult::Succeeded;
}

void UMoveToPlayerTask::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	
	if (ActualPosition == AI->GetActorLocation())
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
	}

	ActualPosition = AI->GetActorLocation();
}



