#pragma once

#include "BehaviorTree/Tasks/BTTask_MoveTo.h"
#include "MoveToPlayerTask.generated.h"

UCLASS()
class GLADIATORGP2019_API UMoveToPlayerTask : public UBTTask_MoveTo
{
	GENERATED_BODY()

		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;


public:
	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector PlayerKey;

	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector AIKey;

private:
	UPROPERTY(EditAnywhere, Category = Rotation)
		float Distance = 400.0f;

	FVector ActualPosition = FVector::ZeroVector;
	ACharacter * AI = nullptr;
};
