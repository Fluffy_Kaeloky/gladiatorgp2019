#include "GladiatorGP2019.h"
#include "AttackTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AttackManager.h"


EBTNodeResult::Type UAttackTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	UBlackboardComponent* BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ACharacter* AI = Cast<ACharacter>(BlackBoard->GetValueAsObject(AIKey.SelectedKeyName));
	if (AI == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	UAttackManager * AIAttackManager = AI->FindComponentByClass<UAttackManager>();
	if (AIAttackManager == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	FTimerHandle timerHandle;
	AIAttackManager->OnAttackPressed();
	GetWorld()->GetTimerManager().SetTimer(timerHandle, [AIAttackManager]() {AIAttackManager->OnAttackReleased();}, 0.5f, false);

	return EBTNodeResult::Succeeded;
}
