#include "GladiatorGP2019.h"
#include "IsAtTheGoodDistance.h"
#include "BehaviorTree/BlackboardComponent.h"


bool UIsAtTheGoodDistance::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{
	AController* Controller = Cast<AController>(OwnerComp.GetOwner());
	AActor* AI = (Controller != nullptr) ? Controller->GetPawn() : nullptr;
	UBlackboardComponent* BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard == nullptr || AI == nullptr)
	{
		return false;
	}

	AActor* Player = Cast<AActor>(BlackBoard->GetValueAsObject(PlayerKey.SelectedKeyName));
	if (Player == nullptr)
	{
		return false;
	}

	

	return (Player->GetDistanceTo(AI) <= Distance);
}
