#pragma once

#include "Components/ActorComponent.h"
#include "AttackManager.generated.h"


class AEnemyChar;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GLADIATORGP2019_API UAttackManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	UAttackManager();

	//virtual void TickComponent(float DeltaTime, ElevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* skeletalMeshComponent = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* weaponMesh = nullptr;

	UPROPERTY(EditAnywhere)
	FName startSocketRaycastWeaponName = "StartRaycast";
	
	UPROPERTY(EditAnywhere)
	FName endSocketRaycastWeaponName = "EndRaycast";

	UPROPERTY(EditAnywhere)
	float damages = 5.0f;

	UFUNCTION(BlueprintCallable, Category = "AttackManager")
	void OnAttackPressed();
	
	UFUNCTION(BlueprintCallable, Category = "AttackManager")
	void OnAttackReleased();

	void OnLockPressed();
	void OnLockSelected();

	UFUNCTION(BlueprintCallable, Category="MeleeHit")
	void ApplyDamageRaycast();

	void UpdateLock();

private:
	bool unlock = true;

	TArray<AEnemyChar*> enemyArray;

	AEnemyChar* enemy = nullptr;
	AEnemyChar* player = nullptr;
};
