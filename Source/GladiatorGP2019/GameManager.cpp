#include "GladiatorGP2019.h"
#include "GameManager.h"
#include "PlayerChar.h"
#include "LifeManager.h"
#include "AI/AIDirector.h"

AGameManager::AGameManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGameManager::BeginPlay()
{
	Super::BeginPlay();
	if (!aiDirector)
	{
		UE_LOG(LogTemp, Error, TEXT("Check your configuration in GameManager."));
		return;
	}

	aiDirector->onAllAIDead.AddUFunction(this, "OnAllAIsDead");
}

void AGameManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (!playerChar)
	{
		for (FConstPlayerControllerIterator iterator = GetWorld()->GetPlayerControllerIterator(); iterator; ++iterator)
		{
			if ((*iterator) != nullptr) {

				TAutoWeakObjectPtr<APlayerController> pointer = (*iterator);
				
				APlayerController* playerContorller = pointer.Get();

				playerChar = Cast<APlayerChar>(playerContorller->GetPawn());
			}
		}

		if (playerChar)
		{
			ULifeManager* lifeManager = Cast<ULifeManager>(playerChar->GetComponentByClass(ULifeManager::StaticClass()));
			lifeManager->onDeath.AddUFunction(this, "OnPlayerDeath");
		}
	}
}

