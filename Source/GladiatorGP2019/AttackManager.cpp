#include "GladiatorGP2019.h"
#include "AttackManager.h"
#include "DwarfAnimInstancebase.h"
#include "LifeManager.h"
#include "EnemyChar.h"

UAttackManager::UAttackManager()
{
	bWantsBeginPlay = false;
	PrimaryComponentTick.bCanEverTick = false;
}

/*void UAttackManager::TickComponent(float DeltaTime, ElevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (unlock)
	{
		UpdateLock();
	}
}*/

void UAttackManager::OnAttackPressed()
{
	UDwarfAnimInstanceBase* animInstance = Cast<UDwarfAnimInstanceBase>(skeletalMeshComponent->GetAnimInstance());
	if (animInstance)
		animInstance->attacking = true;
}

void UAttackManager::OnAttackReleased()
{
	UDwarfAnimInstanceBase* animInstance = Cast<UDwarfAnimInstanceBase>(skeletalMeshComponent->GetAnimInstance());
	if (animInstance)
		animInstance->attacking = false;

	
	//weaponMesh->DetachFromComponent(FDetachmentTransformRules::LocationRule);
}

/*void UAttackManager::OnLockPressed()
{
	if (unlock)
	{
		UE_LOG(LogTemp, Warning, TEXT("Lock"));

		for (TActorIterator<AEnemyChar> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			AEnemyChar* character = *ActorItr;

			if (!character->Tags.Find("Player"))
			{
				player = character;
			}
			else if (!character->Tags.Find("Enemy"))
			{
				enemy = character;
				enemyArray.Add(enemy);
			}
		}

		unlock = false;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UnLock"));
		unlock = true;
	}
}*/

void UAttackManager::UpdateLock()
{
	for (int i = 0; i < enemyArray.Num(); i++)
	{
		FVector enLoc = enemyArray[i]->GetActorLocation();

		FRotator rotPlayer = FRotationMatrix::MakeFromX(enLoc - player->GetActorLocation()).Rotator();

		player->SetActorRotation(rotPlayer);
	}
}

void UAttackManager::ApplyDamageRaycast()
{
	if (!weaponMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("No mesh configured for UAttackManager"));
		return;
	}

	FVector startRaycast = weaponMesh->GetSocketLocation(startSocketRaycastWeaponName);
	FVector endRaycast = weaponMesh->GetSocketLocation(endSocketRaycastWeaponName);

	UWorld* world = GetWorld();

	TArray<FHitResult> hitResults;
	world->LineTraceMultiByChannel(hitResults, startRaycast, endRaycast, ECollisionChannel::ECC_Pawn);

	for (int i = 0; i < hitResults.Num(); i++)
	{
		AActor* actor = hitResults[i].Actor.Get();

		if (actor == GetOwner())
			continue;

		UActorComponent* actorComp = actor->GetComponentByClass(ULifeManager::StaticClass());
		if (actorComp)
		{
			ULifeManager* lifeManager = Cast<ULifeManager>(actorComp);
			if (lifeManager)
				lifeManager->TakeDamage(damages);
		}
	}
}