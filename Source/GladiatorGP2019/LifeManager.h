#pragma once

#include "Components/ActorComponent.h"
#include "LifeManager.generated.h"

DECLARE_MULTICAST_DELEGATE(FLifeManagerOnDeathSignature);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GLADIATORGP2019_API ULifeManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	ULifeManager();

	virtual void BeginPlay() override;
	
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	void TakeDamage(float DamageAmount);

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetLife();
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetLife(float _life);

	FLifeManagerOnDeathSignature onDeath;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* skeletalMeshComp = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool isDead = false;

private:
	UPROPERTY(EditAnywhere)
	unsigned int Life = 100;

	APawn* pawn = nullptr;
};
