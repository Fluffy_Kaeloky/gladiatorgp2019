#include "GladiatorGP2019.h"
#include "EnemyChar.h"
#include "AI/AICrowdFollowingComponent.h"
#include "AttackManager.h"
#include "DamageFeedbackComponent.h"
#include "PlayerMovementController.h"


AEnemyChar::AEnemyChar()
{
	PrimaryActorTick.bCanEverTick = true;

	enemyMovementController = CreateDefaultSubobject<UPlayerMovementController>(TEXT("EnemyMovementController"));
	enemyLifeManager = CreateDefaultSubobject<ULifeManager>(TEXT("EnemyLifeManager"));
	enemyLifeManager->onDeath.AddUFunction(this, "OnDeath");
	enemyAttackManager = CreateDefaultSubobject<UAttackManager>(TEXT("EnemyAttackManager"));
	damageFeedbackComponent = CreateDefaultSubobject<UDamageFeedbackComponent>(TEXT("DamageFeedbackComponent"));
	AICrowdFollowingComponent = CreateDefaultSubobject<UAICrowdFollowingComponent>(TEXT("AICrowdFollowingComponent"));
}

void AEnemyChar::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyChar::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

float AEnemyChar::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (ActualDamage > 0.f)
		enemyLifeManager->TakeDamage(DamageAmount);

	return ActualDamage;
}

void AEnemyChar::OnDeath()
{
	GetController()->UnPossess();
}

void AEnemyChar::TurnOff()
{
	APawn::TurnOff();
}

