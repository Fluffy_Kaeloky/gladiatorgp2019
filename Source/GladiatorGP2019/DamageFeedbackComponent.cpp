#include "GladiatorGP2019.h"
#include "DamageFeedbackComponent.h"

UDamageFeedbackComponent::UDamageFeedbackComponent()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}

void UDamageFeedbackComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!materialTemplate || !meshInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("DamageFeedbackComponent incorrectly configured."));
		return;
	}

	dynamicMaterialInstance = UMaterialInstanceDynamic::Create(materialTemplate, this);

	meshInstance->SetMaterial(materialIndex, dynamicMaterialInstance);
}

void UDamageFeedbackComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (redStatus > 0.0f)
		redStatus -= DeltaTime * 2.0f;
	else
		redStatus = 0.0f;

	dynamicMaterialInstance->SetVectorParameterValue(TEXT("DamageFeedback"), FLinearColor(1.0f, 1.0f - redStatus, 1.0f - redStatus, 1.0f));
}

void UDamageFeedbackComponent::PlayFeedback()
{
	redStatus = 1.0f;
}
