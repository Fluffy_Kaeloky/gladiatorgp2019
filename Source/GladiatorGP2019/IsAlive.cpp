#include "GladiatorGP2019.h"
#include "IsAlive.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "LifeManager.h"



bool UIsAlive::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	UBlackboardComponent* BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard == nullptr)
	{
		return false;
	}

	AActor* Player = Cast<AActor>(BlackBoard->GetValueAsObject(PlayerKey.SelectedKeyName));
	if (Player == nullptr)
	{
		return false;
	}

	ULifeManager* PlayerLifeManager = Player->FindComponentByClass<ULifeManager>();

	return (!PlayerLifeManager->isDead);
}
