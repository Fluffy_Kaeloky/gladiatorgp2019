#include "GladiatorGP2019.h"
#include "LifeManager.h"
#include "DamageFeedbackComponent.h"
#include "DwarfAnimInstancebase.h"

ULifeManager::ULifeManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}


void ULifeManager::BeginPlay()
{
	Super::BeginPlay();

	pawn = Cast<APawn>(GetOwner());
}


void ULifeManager::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

void ULifeManager::TakeDamage(float _damageAmount)
{
	if (isDead)
		return;

	if (Life <= _damageAmount)
	{
		Life = 0.0f;
		isDead = true;

		UE_LOG(LogTemp, Warning, TEXT("Firing ULifeManager::onDeathEvent ..."));

		UDwarfAnimInstanceBase* animInstance = Cast<UDwarfAnimInstanceBase>(skeletalMeshComp->GetAnimInstance());
		if (animInstance)
			animInstance->death = true;

		onDeath.Broadcast();
	}
	else
		Life -= _damageAmount;

	UActorComponent* baseComp = GetOwner()->GetComponentByClass(UDamageFeedbackComponent::StaticClass());
	if (!baseComp)
		return;

	UDamageFeedbackComponent* damageFeedbackComp = Cast<UDamageFeedbackComponent>(baseComp);

	if (damageFeedbackComp != nullptr)
		damageFeedbackComp->PlayFeedback();
}

float ULifeManager::GetLife()
{
	return Life;
}

void ULifeManager::SetLife(float _life)
{
	Life = _life;
}