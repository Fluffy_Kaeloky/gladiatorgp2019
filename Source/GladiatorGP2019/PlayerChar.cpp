#include "GladiatorGP2019.h"
#include "PlayerChar.h"
#include "PlayerMovementController.h"
#include "AttackManager.h"
#include "DamageFeedbackComponent.h"

APlayerChar::APlayerChar()
{
	PrimaryActorTick.bCanEverTick = false;

	playerMovementController = CreateDefaultSubobject<UPlayerMovementController>(TEXT("PlayerMovementController"));
	playerLifeManager = CreateDefaultSubobject<ULifeManager>(TEXT("PlayerLifeManager"));
	attackManager = CreateDefaultSubobject<UAttackManager>(TEXT("AttackManager"));
	damageFeedbackComponent = CreateDefaultSubobject<UDamageFeedbackComponent>(TEXT("DamageFeedbackComponent"));
}

void APlayerChar::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("Vertical", playerMovementController, &UPlayerMovementController::MoveForward);
	InputComponent->BindAxis("Horizontal", playerMovementController, &UPlayerMovementController::MoveRight);

	InputComponent->BindAxis("MouseX", playerMovementController, &UPlayerMovementController::MouseMoveX);
	InputComponent->BindAxis("MouseY", playerMovementController, &UPlayerMovementController::MouseMoveY);

	InputComponent->BindAction("Fire1", EInputEvent::IE_Pressed, attackManager, &UAttackManager::OnAttackPressed);
	InputComponent->BindAction("Fire1", EInputEvent::IE_Released, attackManager, &UAttackManager::OnAttackReleased);

	//InputComponent->BindAction("Fire2", EInputEvent::IE_Pressed, attackManager, &UAttackManager::OnLockPressed);
}

