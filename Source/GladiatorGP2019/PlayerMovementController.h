#pragma once

#include "Components/ActorComponent.h"
#include "PlayerMovementController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GLADIATORGP2019_API UPlayerMovementController : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent* movementRelativeTo = nullptr;

	UPROPERTY(EditAnywhere, Category = Rotation)
	float Distance = 400.0f;

	UPROPERTY(EditAnywhere, Category = Rotation)
	float MoveSpeed = 150.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movements)
	bool canMove = true;

public:	
	UPlayerMovementController();

	virtual void BeginPlay() override;
	
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	void MoveForward(float _axis);
	void MoveRight(float _axis);

	void MouseMoveX(float _axis);
	void MouseMoveY(float _axis);

private:
	APawn* pawn = nullptr;
};
