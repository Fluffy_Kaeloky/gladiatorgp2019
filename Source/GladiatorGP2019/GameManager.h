#pragma once

#include "GameFramework/Actor.h"
#include "GameManager.generated.h"

class AAIDirector;
class APlayerChar;

UCLASS()
class GLADIATORGP2019_API AGameManager : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	AAIDirector* aiDirector = nullptr;

	UPROPERTY(VisibleAnywhere, Transient)
	APlayerChar* playerChar = nullptr;
public:	
	AGameManager();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerDeath();

	UFUNCTION(BlueprintImplementableEvent)
	void OnAllAIsDead();
};
