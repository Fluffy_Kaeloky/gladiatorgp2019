#pragma once

#include "Animation/AnimNotifies/AnimNotify.h"
#include "DwarfHitNotify.generated.h"

UCLASS()
class GLADIATORGP2019_API UDwarfHitNotify : public UAnimNotify
{
	GENERATED_BODY()

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
