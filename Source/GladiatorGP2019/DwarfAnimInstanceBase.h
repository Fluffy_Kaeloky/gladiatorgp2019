#pragma once

#include "Animation/AnimInstance.h"
#include "DwarfAnimInstanceBase.generated.h"

UCLASS()
class GLADIATORGP2019_API UDwarfAnimInstanceBase : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool attacking = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool death = false;
	

	UDwarfAnimInstanceBase(const FObjectInitializer& ObjectInitializer);
};
