#pragma once

#include "BehaviorTree/BTDecorator.h"
#include "IsAlive.generated.h"

UCLASS()
class GLADIATORGP2019_API UIsAlive : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	
	UPROPERTY(EditAnywhere)
		FBlackboardKeySelector PlayerKey;
};
