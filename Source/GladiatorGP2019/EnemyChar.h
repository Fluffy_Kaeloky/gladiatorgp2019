#pragma once

#include "GameFramework/Character.h"
#include "LifeManager.h"
#include "EnemyChar.generated.h"

class UAICrowdFollowingComponent;
class UAttackManager;
class UDamageFeedbackComponent;
class UPlayerMovementController;

UCLASS()
class GLADIATORGP2019_API AEnemyChar : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyChar();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, AController * EventInstigator,	AActor * DamageCauser) override;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
		ULifeManager* enemyLifeManager = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
		UAttackManager * enemyAttackManager = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
		UPlayerMovementController* enemyMovementController = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
		UDamageFeedbackComponent* damageFeedbackComponent = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
		UAICrowdFollowingComponent * AICrowdFollowingComponent = nullptr;

	UFUNCTION(BlueprintCallable, Category = "EnemyChar")
		void TurnOff();
	
private:

	UPROPERTY(EditAnywhere)
		unsigned int AttackDamage = 10;

	UFUNCTION()
		void OnDeath();
	
};
