#include "GladiatorGP2019.h"
#include "PlayerMovementController.h"


UPlayerMovementController::UPlayerMovementController()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}


void UPlayerMovementController::BeginPlay()
{
	Super::BeginPlay();

	pawn = Cast<APawn>(GetOwner());
}


void UPlayerMovementController::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

void UPlayerMovementController::MoveForward(float _axis)
{
	if (!canMove)
		return;

	FVector vector;

	if (movementRelativeTo != nullptr)
	{
		vector = movementRelativeTo->GetForwardVector();

		vector = FVector::VectorPlaneProject(vector, FVector::UpVector);
		vector.Normalize();

		if (vector.Size() <= 0.1f)
			vector = movementRelativeTo->GetUpVector();
	}
	else
		vector = pawn->GetActorForwardVector();

	pawn->AddMovementInput(vector, _axis);
}

void UPlayerMovementController::MoveRight(float _axis)
{
	if (!canMove)
		return;

	FVector vector;

	if (movementRelativeTo != nullptr)
	{
		vector = movementRelativeTo->GetRightVector();
		vector.Normalize();
	}
	else
		vector = pawn->GetActorRightVector();

	pawn->AddMovementInput(vector, _axis);
}

void UPlayerMovementController::MouseMoveX(float _axis)
{
	pawn->AddControllerYawInput(_axis);
}

void UPlayerMovementController::MouseMoveY(float _axis)
{
	pawn->AddControllerPitchInput(-_axis);
}
