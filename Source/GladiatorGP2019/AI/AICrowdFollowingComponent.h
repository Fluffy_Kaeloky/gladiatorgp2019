#pragma once

#include "Navigation/CrowdFollowingComponent.h"
#include "AICrowdFollowingComponent.generated.h"

UCLASS()
class GLADIATORGP2019_API UAICrowdFollowingComponent : public UCrowdFollowingComponent
{
	GENERATED_BODY()
	
public:
	UAICrowdFollowingComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	
};
