#pragma once

#include "AIController.h"
#include "MyAIController.generated.h"


UCLASS()
class GLADIATORGP2019_API AMyAIController : public AAIController
{
	GENERATED_BODY()

public:
		AMyAIController(const FObjectInitializer& ObjectInitializer);
	
};
