#include "GladiatorGP2019.h"
#include "AICrowdFollowingComponent.h"

UAICrowdFollowingComponent::UAICrowdFollowingComponent()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}

void UAICrowdFollowingComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UAICrowdFollowingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
