#pragma once

#include "GameFramework/Actor.h"
#include "AIDirector.generated.h"

DECLARE_MULTICAST_DELEGATE(FAAIDirectorAllAIDeadSignature);

UCLASS()
class GLADIATORGP2019_API AAIDirector : public AActor
{
	GENERATED_BODY()
	
public:	
	AAIDirector();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	UClass*	AIClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	TArray<AActor*>	SpawnPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float	GoToPlayerDelay = 1.0f;

	UFUNCTION()
	void OnAIDeath();

	FAAIDirectorAllAIDeadSignature onAllAIDead;

	TArray<ACharacter*> GetAIPawns() { return AIPawns; }

private:
	void	InitAI(ACharacter* AI);

	void	UpdateGotoPlayer();

private:
	TArray<ACharacter*>	AIPawns;

	int32			currentAI = 0;
	FTimerHandle	gotoPlayerTimerHandle;
	
	int aiCountInGame = 0;
	int deadAICount = 0;
};
