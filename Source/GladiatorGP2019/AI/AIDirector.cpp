#include "GladiatorGP2019.h"
#include "AIDirector.h"
#include "EnemyChar.h"
#include "AIController.h"
#include "PlayerMovementController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "LifeManager.h"

AAIDirector::AAIDirector()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AAIDirector::BeginPlay()
{
	Super::BeginPlay();
	
	if (AIClass == nullptr)
	{
		return;
	}

	AEnemyChar* DefaultAI = AIClass->GetDefaultObject<AEnemyChar>();
	float offset = DefaultAI->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

	AIPawns.SetNum(SpawnPoints.Num());
	for (size_t i = 0; i < SpawnPoints.Num(); ++i)
	{
		FVector Location = SpawnPoints[i]->GetActorLocation();
		Location.Z += offset;

		FRotator Rotation = SpawnPoints[i]->GetActorRotation();
		FActorSpawnParameters Params;
		Params.SpawnCollisionHandlingOverride;

		AIPawns[i] = Cast<ACharacter>(GetWorld()->SpawnActor(AIClass, &Location, &Rotation, Params));
		AIPawns[i]->SpawnDefaultController();

		ULifeManager* lifeManager = Cast<ULifeManager>(AIPawns[i]->GetComponentByClass(ULifeManager::StaticClass()));
		if (lifeManager)
		{
			aiCountInGame++;
			lifeManager->onDeath.AddUFunction(this, FName("OnAIDeath"));
		}

		InitAI(AIPawns[i]);
	}

	UpdateGotoPlayer();
}

void AAIDirector::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void	AAIDirector::InitAI(ACharacter* AI)
{
	if (AI == nullptr)
		return;

	AAIController*	AIController = Cast<AAIController>(AI->GetController());
	if (AIController == nullptr)
		return;

	UBlackboardComponent* BlackBoard = AIController->FindComponentByClass<UBlackboardComponent>();
	if (BlackBoard == nullptr)
		return;
	BlackBoard->SetValueAsObject("Player", UGameplayStatics::GetPlayerPawn(this, 0));
}

void	AAIDirector::UpdateGotoPlayer()
{
	for (int32 i = 0; i < AIPawns.Num(); ++i)
	{
		ACharacter* AI = AIPawns[i];
		if (AI == nullptr)
			continue;
		AAIController*	AIController = Cast<AAIController>(AI->GetController());
		if (AIController == nullptr)
			continue;
		UBlackboardComponent* BlackBoard = AIController->FindComponentByClass<UBlackboardComponent>();
		if (BlackBoard == nullptr)
			continue;
		BlackBoard->SetValueAsBool("GotoPlayer", i == currentAI);
	}

	currentAI = (currentAI + 1) % AIPawns.Num();
	GetWorld()->GetTimerManager().SetTimer(gotoPlayerTimerHandle, this, &AAIDirector::UpdateGotoPlayer, 1.0f, false, GoToPlayerDelay);
}


void AAIDirector::OnAIDeath()
{
	deadAICount++;

	//UE_LOG(LogTemp, Warning, TEXT("AAIDirector::OnAIDeath called. : %d AIs are dead."), deadAICount);

	if (deadAICount >= aiCountInGame)
	{
		//UE_LOG(LogTemp, Warning, TEXT("All AIs are dead !"));
		onAllAIDead.Broadcast();
	}
}
