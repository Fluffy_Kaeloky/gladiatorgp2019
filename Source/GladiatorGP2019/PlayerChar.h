#pragma once

#include "GameFramework/Character.h"
#include "LifeManager.h"
#include "PlayerChar.generated.h"

class UPlayerMovementController;
class UAttackManager;
class UDamageFeedbackComponent;

UCLASS()
class GLADIATORGP2019_API APlayerChar : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerChar();

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
	UPlayerMovementController* playerMovementController = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
	ULifeManager* playerLifeManager = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
	UAttackManager* attackManager = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
	UDamageFeedbackComponent* damageFeedbackComponent = nullptr;

private:

	UPROPERTY(EditAnywhere)
	unsigned int attack = 10;
};